//
//  DataUtils.swift
//  LEDlab
//
//  Created by LEDlab on 6/29/17.
//  Copyright © 2017 LEDlab. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class DataUtils {
    static func uploadData() -> Void {
        var userDefaults = UserDefaults.standard.dictionaryRepresentation()
        if userDefaults["UPLOADED_DEVICE_DATA"] == nil {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss"
            DataUtils.uploadDeviceData(pid: userDefaults["PARTICIPANT_ID"] as! String, loginDate: dateFormatter.string(from: userDefaults["LOGIN_DATE"] as! Date))
        }
        DataUtils.uploadSurveyData()
    }

    static func uploadDeviceData(pid: String, loginDate: String) -> Void {
        var parameters = Parameters()
        let uiDevice = UIDevice()
        parameters["participantid"] = pid
        parameters["logindate"] = loginDate
        parameters["name"] = uiDevice.name
        parameters["systemname"] = uiDevice.systemName
        parameters["systemversion"] = uiDevice.systemVersion
        parameters["model"] = uiDevice.model
        parameters["localizedmodel"] = uiDevice.localizedModel
        parameters["userinterfaceidiom"] = {
            let userInterfaceIdiom = uiDevice.userInterfaceIdiom
            var userInterfaceIdiomAsString = ""
            switch userInterfaceIdiom {
                case UIUserInterfaceIdiom.phone:
                    userInterfaceIdiomAsString = "iPhone and iPod touch"
                case UIUserInterfaceIdiom.pad:
                    userInterfaceIdiomAsString = "iPad"
                case UIUserInterfaceIdiom.tv:
                    userInterfaceIdiomAsString = "tvOS and Apple TV"
                case UIUserInterfaceIdiom.carPlay:
                    userInterfaceIdiomAsString = "in-car experience"
                default:
                    userInterfaceIdiomAsString = "unspecified"
            }
            return userInterfaceIdiomAsString
        }()
        parameters["identifierforvendor"] = uiDevice.identifierForVendor?.uuidString
        Alamofire.request("https://script.google.com/macros/s/AKfycbwMoZQJD91MKf2a347XJoDWCkWwutxDDm5gUP-_725E-Fe2fX4/exec", method: .post, parameters: parameters, encoding: JSONEncoding(options: [])).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                var responseJSON = JSON(value)
                if responseJSON["status"] == true {
                    UserDefaults.standard.set(true, forKey: "UPLOADED_DEVICE_DATA")
                }
            case .failure(_):
                print("Device Data Upload : Request Failed")
            }
        }
    }
    
    static func uploadSurveyData() -> Void {
        let dbUtils = DBUtils()
        let surveys = dbUtils.getDataForUpload()
        if surveys.count > 0 {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss"
            let userDefaults = UserDefaults.standard.dictionaryRepresentation()
            var parameters = Parameters()
            var surveyIds = [Int]()
            parameters["participantid"] = userDefaults["PARTICIPANT_ID"] as! String
            var surveyResponses = [Dictionary<String, Any>]()
            for survey in surveys {
                var tempSurvey = [String: Any]()
                tempSurvey["surveyid"] = String(survey.surveyID)
                surveyIds.append(survey.surveyID)
                tempSurvey["createddate"] = dateFormatter.string(from: survey.createdDate as Date)
                var responses = [Dictionary<String, String>]()
                for response in survey.responses {
                    responses.append(["tag": response.tag, "value": response.value])
                }
                tempSurvey["responses"] = responses
                surveyResponses.append(tempSurvey)
            }
            parameters["surveys"] = surveyResponses

            Alamofire.request("https://script.google.com/macros/s/AKfycbyTyRWC5Gfk49Qcc9mP7NiLJY8MgwnD3EhlDQS4YkP3mvv38FU/exec", method: .post, parameters: parameters, encoding: JSONEncoding(options: [])).validate().responseJSON { response in
                switch response.result {
                    case .success(let value):
                        var responseJSON = JSON(value)
                        if responseJSON["status"] == true {
                            dbUtils.updateUploadDate(surveyIds: surveyIds)
                        }
                    case .failure(_):
                        print("Survey Response Upload : Request Failed")
                }
            }
        } else {
            print("No surveys found to be Upload")
        }
    }
    
    static func emailSurveyData() -> String {
        var csvString = ""
        let surveys = DBUtils().getDataForEmail()
        if surveys.count > 0 {
            csvString += "Survey ID,Survey Date,Tag,Response\n"
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss"
            for survey in surveys {
                let surveyId = String(survey.surveyID)
                let createdDate = dateFormatter.string(from: survey.createdDate as Date)
                for response in survey.responses {
                    csvString += "\"\(surveyId)\",\"\(createdDate)\",\"\(response.tag)\",\"\(response.value.replacingOccurrences(of: "\"", with: ""))\"\n"
                }
            }
        }
        return csvString
    }
}
