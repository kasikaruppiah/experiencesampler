//
//  SettingsViewController.swift
//  LEDlab
//
//  Created by LEDlab on 6/25/17.
//  Copyright © 2017 LEDlab. All rights reserved.
//

import UIKit
import Eureka
import UserNotifications

class SettingsViewController: FormViewController {
    
    var oldValues = [String: Any]()
    var newValues = [String: Any]()
    var settings = ["WEEKDAY_STARTTIME", "WEEKDAY_STOPTIME", "WEEKEND_STARTTIME", "WEEKEND_STOPTIME"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        getUserDefaults()
        generateSettingsForm()
        loadFormValues()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func generateSettingsForm() -> Void {
        form
            +++ Section()
                <<< SwitchRow("ENABLE_NOTIFICATION") {
                    $0.title = "Display Notifications"
                }.onChange {row in
                    self.newValues["ENABLE_NOTIFICATION"] = row.value!
                    self.updateActionButtons()
                }
            +++ Section("Sleep Cycle") {
                    $0.hidden = .function(["ENABLE_NOTIFICATION"], { form -> Bool in
                        let row: RowOf<Bool>! = form.rowBy(tag: "ENABLE_NOTIFICATION")
                        return row.value ?? false == false
                    })
                }
                <<< LabelRow() {
                    $0.title = "Weekday"
                }
                <<< TimeInlineRow("WEEKDAY_STARTTIME") {
                    $0.title = "Wake Up Time"
                }.onChange{row in
                    self.newValues["WEEKDAY_STARTTIME"] = row.value!
                    self.updateActionButtons()
                }
                <<< TimeInlineRow("WEEKDAY_STOPTIME") {
                    $0.title = "Bed Time"
                }.onChange{row in
                    self.newValues["WEEKDAY_STOPTIME"] = row.value!
                    self.updateActionButtons()
                }
                <<< LabelRow() {
                    $0.title = "Weekend"
                }
                <<< TimeInlineRow("WEEKEND_STARTTIME") {
                    $0.title = "Wake Up Time"
                }.onChange{row in
                    self.newValues["WEEKEND_STARTTIME"] = row.value!
                    self.updateActionButtons()
                }
                <<< TimeInlineRow("WEEKEND_STOPTIME") {
                    $0.title = "Bed Time"
                }.onChange{row in
                    self.newValues["WEEKEND_STOPTIME"] = row.value!
                    self.updateActionButtons()
                }
            +++ Section()
                <<< ButtonRow("UPDATE_BUTTON") {
                    $0.title = "Update Notifications"
                    $0.onCellSelection() { _, _ in
                        self.saveFormValues()
                        self.disableActionButtons()
                    }
                }
                <<< ButtonRow("RESTORE_BUTTON") {
                    $0.title = "Restore"
                    $0.onCellSelection() { _, _ in
                        self.loadFormValues()
                    }
                }
            +++ Section()
    }
    
    func saveFormValues() -> Void {
        var saveSettings = ["ENABLE_NOTIFICATION"]
        if (newValues["ENABLE_NOTIFICATION"] as! Bool == true) {
            saveSettings += self.settings
        }
        for setting in saveSettings {
            UserDefaults.standard.set(newValues[setting], forKey: setting)
            self.oldValues[setting] = newValues[setting]
        }
        NotificationUtils.updateNotification(enabled: newValues["ENABLE_NOTIFICATION"] as! Bool)
    }

    func getUserDefaults() -> Void {
        var userDefaults = UserDefaults.standard.dictionaryRepresentation()
        for setting in ["ENABLE_NOTIFICATION"] + settings {
            self.oldValues[setting] = userDefaults[setting]
        }
    }

    func loadFormValues() -> Void {
        form.setValues([
            "ENABLE_NOTIFICATION": self.oldValues["ENABLE_NOTIFICATION"] as! Bool,
            "WEEKDAY_STARTTIME": self.oldValues["WEEKDAY_STARTTIME"] as! Date,
            "WEEKDAY_STOPTIME": self.oldValues["WEEKDAY_STOPTIME"] as! Date,
            "WEEKEND_STARTTIME": self.oldValues["WEEKEND_STARTTIME"] as! Date,
            "WEEKEND_STOPTIME": self.oldValues["WEEKEND_STOPTIME"] as! Date
        ])
        tableView.reloadData()
    }

    func updateActionButtons() -> Void {
        var status = false
        if NSDictionary(dictionary: self.oldValues).isEqual(to: newValues) {
            status = true
        }
        if status {
            self.disableActionButtons()
        } else {
            self.enableActionButtons()
        }
    }

    func disableActionButtons() -> Void {
        for tag in ["UPDATE_BUTTON", "RESTORE_BUTTON"] {
            let row = form.rowBy(tag: tag)
            row?.disabled = true
            row?.evaluateDisabled()
        }
    }

    func enableActionButtons() -> Void {
        for tag in ["UPDATE_BUTTON", "RESTORE_BUTTON"] {
            let row = form.rowBy(tag: tag)
            row?.disabled = false
            row?.evaluateDisabled()
        }
    }
}
