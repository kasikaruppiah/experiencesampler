//
//  UploadViewController.swift
//  LEDlab
//
//  Created by LEDlab on 6/25/17.
//  Copyright © 2017 LEDlab. All rights reserved.
//

import UIKit
import MessageUI

class UploadViewController: UIViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var offlineLabel: UILabel!
    @IBOutlet weak var uploadButton: UIButton!
    @IBOutlet weak var emailButton: UIButton!
    @IBAction func sendEmail(_ sender: Any) {
        let userDefaults = UserDefaults.standard.dictionaryRepresentation()
        let participantId = userDefaults["PARTICIPANT_ID"] as! String
        let csvString = DataUtils.emailSurveyData()
        let csvNSString = csvString as NSString
        let csvNSData = csvNSString.data(using: String.Encoding.utf8.rawValue)
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        composeVC.setToRecipients(["ledlabneu@gmail.com"])
        composeVC.setSubject("\(participantId) - Survey Responses")
        composeVC.setMessageBody("<p>Hi,</p><p>Please find attached csv(\(participantId).csv) containing my survey responses.</p><p>Thanks,<br>\(participantId)</p>", isHTML: true)
        composeVC.addAttachmentData(csvNSData!, mimeType: "text/csv", fileName: "\(participantId).csv")

        present(composeVC, animated: true, completion: nil)
    }

    @IBAction func uploadData(_ sender: Any) {
        DataUtils.uploadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        let dbUtils = DBUtils()
        let offlineSurveyCount = dbUtils.getOfflineSurveyCount()
        let totalSurveyCount = dbUtils.getTotalSurveyCount()
        offlineLabel.text = "Offline Surveys : \(offlineSurveyCount) / \(totalSurveyCount)"
        if offlineSurveyCount == 0 {
            uploadButton.isEnabled = false
        } else {
            uploadButton.isEnabled = true
        }
        if MFMailComposeViewController.canSendMail() && totalSurveyCount != 0 {
            emailButton.isEnabled = true
        } else {
            emailButton.isEnabled = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }

}
