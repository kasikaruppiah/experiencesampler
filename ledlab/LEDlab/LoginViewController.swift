//
//  LoginViewController.swift
//  LEDlab
//
//  Created by LEDlab on 6/29/17.
//  Copyright © 2017 LEDlab. All rights reserved.
//

import UIKit
import Eureka

class LoginViewController: FormViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        saveDefaultNotificationTime()
        generateLoginForm()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func generateLoginForm() -> Void {
        var userDefaults = UserDefaults.standard.dictionaryRepresentation()
        form
            +++ Section()
                <<< TextRow("PARTICIPANT_ID") {
                    $0.title = "Participant ID"
                    $0.placeholder = "#####"
                }.onChange {row in
                    let loginButtonRow = self.form.rowBy(tag: "LOGIN")
                    if row.value != nil {
                        loginButtonRow?.disabled = false
                    } else {
                        loginButtonRow?.disabled = true
                    }
                    loginButtonRow?.evaluateDisabled()
                }
                <<< SwitchRow("ENABLE_NOTIFICATION") {
                    $0.title = "Display Notifications"
                    $0.value = true
                }
            +++ Section("Sleep Cycle") {
                    $0.hidden = .function(["ENABLE_NOTIFICATION"], { form -> Bool in
                        let row: RowOf<Bool>! = form.rowBy(tag: "ENABLE_NOTIFICATION")
                        return row.value ?? false == false
                    })
                }
                <<< LabelRow() {
                    $0.title = "Weekday"
                }
                <<< TimeInlineRow("WEEKDAY_STARTTIME") {
                    $0.title = "Wake Up Time"
                    $0.value = userDefaults["WEEKDAY_STARTTIME"] as? Date
                }
                <<< TimeInlineRow("WEEKDAY_STOPTIME") {
                    $0.title = "Bed Time"
                    $0.value = userDefaults["WEEKDAY_STOPTIME"] as? Date
                }
                <<< LabelRow() {
                    $0.title = "Weekend"
                }
                <<< TimeInlineRow("WEEKEND_STARTTIME") {
                    $0.title = "Wake Up Time"
                    $0.value = userDefaults["WEEKEND_STARTTIME"] as? Date
                }
                <<< TimeInlineRow("WEEKEND_STOPTIME") {
                    $0.title = "Bed Time"
                    $0.value = userDefaults["WEEKEND_STOPTIME"] as? Date
                }
            +++ Section()
                <<< ButtonRow("LOGIN") {
                    $0.title = "Login"
                    $0.disabled = true
                    $0.onCellSelection() { _, _ in
                        self.saveFormValues()
                        let row: TextRow? = self.form.rowBy(tag: "PARTICIPANT_ID")
                        let pid = row?.value as! String
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss"
                        DataUtils.uploadDeviceData(pid: pid, loginDate: dateFormatter.string(from: Date()))
                        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Tab")
                        
                        self.present(viewController, animated:true, completion:nil)
                    }
                }
            +++ Section()
    }
    
    func saveDefaultNotificationTime() -> Void {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        UserDefaults.standard.set(dateFormatter.date(from: "05:00"), forKey: "WEEKDAY_STARTTIME")
        UserDefaults.standard.set(dateFormatter.date(from: "21:00"), forKey: "WEEKDAY_STOPTIME")
        UserDefaults.standard.set(dateFormatter.date(from: "08:00"), forKey: "WEEKEND_STARTTIME")
        UserDefaults.standard.set(dateFormatter.date(from: "00:00"), forKey: "WEEKEND_STOPTIME")
    }

    func saveFormValues() -> Void {
        var valuesDictionary = form.values()
        valuesDictionary.removeValue(forKey: "LOGIN")
        UserDefaults.standard.set(valuesDictionary["PARTICIPANT_ID"] as! String, forKey: "PARTICIPANT_ID")
        UserDefaults.standard.set(Date(), forKey: "LOGIN_DATE")
        UserDefaults.standard.set(valuesDictionary["ENABLE_NOTIFICATION"] as! Bool, forKey: "ENABLE_NOTIFICATION")
        if (valuesDictionary["ENABLE_NOTIFICATION"] as! Bool == true) {
            UserDefaults.standard.set(valuesDictionary["WEEKDAY_STARTTIME"] as! Date, forKey: "WEEKDAY_STARTTIME")
            UserDefaults.standard.set(valuesDictionary["WEEKDAY_STOPTIME"] as! Date, forKey: "WEEKDAY_STOPTIME")
            UserDefaults.standard.set(valuesDictionary["WEEKEND_STARTTIME"] as! Date, forKey: "WEEKEND_STARTTIME")
            UserDefaults.standard.set(valuesDictionary["WEEKEND_STOPTIME"] as! Date, forKey: "WEEKEND_STOPTIME")
            NotificationUtils.addNotification()
        }
    }

}
