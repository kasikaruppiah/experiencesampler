//
//  QuestionsViewController.swift
//  LEDlab
//
//  Created by LEDlab on 6/26/17.
//  Copyright © 2017 LEDlab. All rights reserved.
//

import UIKit
import Eureka
import SwiftyJSON

class QuestionsViewController: FormViewController {

    var json = FileLib.fileToJson(filename: "survey")
    var formValues = [String: String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadQuestion(id: 0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadQuestion(id: Int) -> Void {
        var questionJson = json["questions"][id]

        form.removeAll()
        LabelRow.defaultCellSetup = { cell, row in
            cell.textLabel?.numberOfLines = 0
        }

        form
            +++ Section()
                <<< LabelRow() {
                    $0.title = questionJson["question"].string
                }

        switch questionJson["type"] {
            case "radio":
                form
                    +++ SelectableSection<ImageCheckRow<String>>("", selectionType: .singleSelection(enableDeselection: false))
                form.last?.tag = questionJson["tag"].string
                for (index, optionJson) in questionJson["options"] {
                    form.last!
                        <<< ImageCheckRow<String>(index) {
                            $0.title = optionJson["option"].string
                            $0.selectableValue = optionJson["value"].string
                            $0.value = nil
                            $0.add(rule: RuleRequired())
                        }.cellSetup { cell, row in
                            cell.textLabel?.numberOfLines = 0
                        }.onChange {_ in
                            self.enableNextButton()
                        }
                }
            case "checkbox":
                form
                    +++ SelectableSection<ImageCheckRow<String>>("", selectionType: .multipleSelection)
                form.last?.tag = questionJson["tag"].string
                for (index, optionJson) in questionJson["options"] {
                    form.last!
                        <<< ImageCheckRow<String>(index) {
                            $0.title = optionJson["option"].string
                            $0.selectableValue = optionJson["value"].string
                            $0.value = nil
                        }.cellSetup {cell, _ in
                            cell.textLabel?.numberOfLines = 0
                            cell.trueImage = UIImage(named: "selectedRectangle")!
                            cell.falseImage = UIImage(named: "unselectedRectangle")!
                        }
                }
            case "text":
                form
                    +++ Section()
                        <<< TextAreaRow(questionJson["tag"].string) {
                            $0.textAreaHeight = .dynamic(initialTextViewHeight: 100)
                        }.onChange {row in
                            if row.value != nil {
                                self.enableNextButton()
                            } else {
                                self.disableNextButton()
                            }
                        }
            default:
                form
                    +++ Section()
        }
        form
            +++ Section()
                <<< ButtonRow("NEXT") {
                    $0.title = "Next"
                    $0.disabled = ["radio", "text"].contains(questionJson["type"]) ? true : false
                    $0.onCellSelection() { cell, row in
                        if !row.isDisabled {
                            self.form.validate()
                            self.processForm(currentQuestion: id)
                        }
                    }
                }
            +++ Section()
    }
    
    func enableNextButton() -> Void {
        let nextButtonRow = form.rowBy(tag: "NEXT")
        nextButtonRow?.disabled = false
        nextButtonRow?.evaluateDisabled()
    }
    
    func disableNextButton() -> Void {
        let nextButtonRow = form.rowBy(tag: "NEXT")
        nextButtonRow?.disabled = true
        nextButtonRow?.evaluateDisabled()
    }

    func processForm(currentQuestion: Int) -> Void {
        var questionJson = json["questions"][currentQuestion]
        var selectedValues = [String]()
        var formValues = form.values()
        formValues.removeValue(forKey: "NEXT")
        var nextQuestion = currentQuestion + 1
        if questionJson["nextquestion"].exists() {
            nextQuestion = Int(questionJson["nextquestion"].string!)!
        }
        let type = questionJson["type"]

        if ["radio", "checkbox"].contains(type) {
            selectedValues = formValues.values.filter { $0 != nil }.map { $0 as! String }
            if selectedValues.count > 0 && questionJson["condition"].exists() {
                for (key, subJson): (String, JSON) in questionJson["condition"] {
                    if selectedValues.contains(key) {
                        nextQuestion = Int(subJson.string!)!
                        break
                    }
                }
            }
        } else if type == "text" {
            selectedValues = formValues.values.map { $0 as! String }
        }

        self.formValues[questionJson["tag"].string!] = selectedValues.joined(separator: ",")
        if nextQuestion < json["questions"].count {
            loadQuestion(id: nextQuestion)
        } else {
            loadLastPage()
        }
    }
    
    func loadLastPage() -> Void {
        form.removeAll()
        LabelRow.defaultCellSetup = { cell, row in
            cell.textLabel?.numberOfLines = 0
        }
        
        form
            +++ Section()
            <<< LabelRow() {
                $0.title = "Thank you! We will send your Experience to the lab."
            }
            +++ Section()
            <<< ButtonRow() {
                $0.title = "Finish"
                $0.onCellSelection() { cell, row in
                    self.dismiss(animated: true, completion: nil)
                }
        }

        DBUtils().addSurvey(userResponse: self.formValues)
        DataUtils.uploadData()
    }
}
