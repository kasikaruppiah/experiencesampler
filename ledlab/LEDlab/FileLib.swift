//
//  FileLib.swift
//  LEDlab
//
//  Created by LEDlab on 6/26/17.
//  Copyright © 2017 LEDlab. All rights reserved.
//

import Foundation
import SwiftyJSON

class FileLib {

    static func fileToJson(filename: String) -> JSON {
        return stringToJson(contents: readFile(filename: filename))
    }

    static func readFile(filename: String) -> String {
        var contents = ""

        if let filepath = Bundle.main.path(forResource: filename, ofType: "json") {
            do {
                contents = try String(contentsOfFile: filepath)
            } catch {
                print("Could Not Load File")
            }
        } else {
            print("File Not Found")
        }

        return contents
    }

    static func stringToJson(contents: String) -> JSON {
        var json:JSON = [:]

        if let dataFromString = contents.data(using: .utf8, allowLossyConversion: false) {
            json = JSON(data: dataFromString)
        }

        return json
    }
}
