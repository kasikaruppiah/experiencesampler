//
//  DBUtils.swift
//  LEDlab
//
//  Created by LEDlab on 6/28/17.
//  Copyright © 2017 LEDlab. All rights reserved.
//

import Foundation
import RealmSwift

class DBUtils {
    let realm = try! Realm()

    func addSurvey(userResponse: [String: String]) -> Void {
        let newSurvey = Survey()
        newSurvey.createdDate = NSDate()
        newSurvey.surveyID = getPrimaryKey(object: newSurvey, property: "surveyID")
        var newResponses = [Response]()
        for (tag, response) in userResponse {
            let tempResponse = Response()
            tempResponse.tag = tag
            tempResponse.value = response
            newResponses.append(tempResponse)
        }
        newSurvey.responses.append(objectsIn: newResponses)

        try! realm.write {
            realm.add(newSurvey)
        }
    }

    func getPrimaryKey(object: Object, property: String) -> Int {
        return (realm.objects(type(of: object)).max(ofProperty: property) as Int? ?? 0) + 1
    }

    func getDataForUpload() -> Results<Survey> {
        return realm.objects(Survey.self).filter("uploadDate == nil")
    }

    func getDataForEmail() -> Results<Survey> {
        return realm.objects(Survey.self)
    }
    
    func updateUploadDate(surveyIds: [Int]) -> Void {
        try! realm.write {
            let surveys = realm.objects(Survey.self).filter("surveyID IN %@", surveyIds)
            surveys.setValue(NSDate(), forKey: "uploadDate")
        }
    }
    
    func getTotalSurveyCount() -> Int {
        return realm.objects(Survey.self).count
    }
    
    func getOfflineSurveyCount() -> Int {
        return realm.objects(Survey.self).filter("uploadDate == nil").count
    }
}
