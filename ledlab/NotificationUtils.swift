//
//  NotificationUtils.swift
//  LEDlab
//
//  Created by LEDlab on 7/4/17.
//  Copyright © 2017 LEDlab. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications

class NotificationUtils {
    static func updateNotification(enabled: Bool) -> Void {
        removeNotification()
        if enabled {
            addNotification()
        }
    }
    
    static func addNotification() -> Void {
        isNotificationEnabled{success in
            if success {
                scheduleLocalNotifications()
            } else {
                print("Notification Disabled")
            }
        }
    }

    static func isNotificationEnabled(completion: @escaping (Bool) -> ()) {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                if settings.authorizationStatus == .authorized {
                    completion(true)
                } else {
                    completion(false)
                }
            }
        } else {
            if let settings = UIApplication.shared.currentUserNotificationSettings {
                if settings.types.intersection([.alert, .badge, .sound]).isEmpty == false {
                    completion(true)
                } else {
                    completion(false)
                }
            }
        }
    }

    static func scheduleLocalNotifications() -> Void {
        let userDefaults = UserDefaults.standard.dictionaryRepresentation()
        let weekdayStartTime = userDefaults["WEEKDAY_STARTTIME"] as? Date
        let weekdayStoptTime = userDefaults["WEEKDAY_STOPTIME"] as? Date
        let weekendStartTime = userDefaults["WEEKEND_STARTTIME"] as? Date
        let weekendStopTime = userDefaults["WEEKEND_STOPTIME"] as? Date
        let weekdayTimeDiff = getTimeDiff(startTime: weekdayStartTime!, stopTime: weekdayStoptTime!)
        let weekendTimeDiff = getTimeDiff(startTime: weekendStartTime!, stopTime: weekendStopTime!)
        let weekdayweekendTimeDiff = getTimeDiff(startTime: weekdayStartTime!, stopTime: weekendStopTime!)
        let weekendweekdayTimeDiff = getTimeDiff(startTime: weekendStartTime!, stopTime: weekdayStoptTime!)

        let calendar = Calendar.current
        let comps: Set<Calendar.Component> = [.calendar, .timeZone, .era, .year, .month, .day, .weekday, .weekdayOrdinal, .quarter, .weekOfMonth, .weekOfYear, .yearForWeekOfYear]
        var dateComp = calendar.dateComponents(comps, from: Date())
        dateComp.hour = 0
        dateComp.minute = 0
        dateComp.second = 0
        dateComp.nanosecond = 0
        let date = calendar.date(from: dateComp)!

        let startDate = date.addingTimeInterval(TimeInterval((1 - dateComp.weekday!) * 24 * 60 * 60))

        for i in 1...7 {
            var weekDayDate = startDate
            if i != 1 {
                weekDayDate = startDate.addingTimeInterval(TimeInterval((i - 1) * 24 * 60 * 60))
            }
            var newDateComp = calendar.dateComponents(comps, from: weekDayDate)
            if [1, 7].contains(i) {
                newDateComp.hour = calendar.component(.hour, from: weekendStartTime!) + 1
                newDateComp.minute = calendar.component(.minute, from: weekendStartTime!)
            } else {
                newDateComp.hour = calendar.component(.hour, from: weekdayStartTime!) + 1
                newDateComp.minute = calendar.component(.minute, from: weekdayStartTime!)
            }
            newDateComp.second = 0
            newDateComp.nanosecond = 0
            var newDate = calendar.date(from: newDateComp)

            var timeDiff: Double = 0
            if [2, 3, 4, 5].contains(i) {
                timeDiff = weekdayTimeDiff
            } else if i == 7 {
                timeDiff = weekendTimeDiff
            } else if i == 1 {
                timeDiff = weekendweekdayTimeDiff
            } else {
                timeDiff = weekdayweekendTimeDiff
            }
            timeDiff *= 15
            for j in 1...5 {
                if j != 1 {
                    newDate = newDate?.addingTimeInterval(timeDiff)
                }
                if newDate! < Date() {
                    newDate = newDate?.addingTimeInterval(7 * 24 * 60 * 60)
                }
                if #available(iOS 10.0, *) {
                    let content = UNMutableNotificationContent()
                    content.title = "LEDlab"
                    content.subtitle = "Survey: \(j) / 5"
                    content.body = "Time for your next survey"
                    content.sound = UNNotificationSound.default()

                    let weeklyTrigger = Calendar.current.dateComponents([.weekday, .hour, .minute, .second, .nanosecond], from: newDate!)
                    let trigger = UNCalendarNotificationTrigger(dateMatching: weeklyTrigger, repeats: true)
                    let identifier = "LELlabLocalNotification_\(i)_\(j)"
                    let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
                    UNUserNotificationCenter.current().add(request, withCompletionHandler: { (error) in
                        if let error = error {
                            print(error)
                        }
                    })
                } else {
                    let localNotification = UILocalNotification()
                    var alertBody = ""
                    if #available(iOS 8.2, *) {
                        localNotification.alertTitle = "LEDlab : Survey: \(j) / 5"
                    } else {
                        alertBody = "LEDlab : Survey: \(j) / 5, "
                    }
                    alertBody += "Time for your next survey"
                    localNotification.alertBody = alertBody
                    localNotification.soundName = UILocalNotificationDefaultSoundName
                    localNotification.fireDate = newDate
                    localNotification.repeatInterval = NSCalendar.Unit.weekOfYear

                    UIApplication.shared.scheduleLocalNotification(localNotification)
                }
            }
        }
    }
    
    static func getTimeDiff(startTime: Date, stopTime: Date) -> Double {
        let calendar = Calendar.current
        let startHour = calendar.component(.hour, from: startTime)
        let startMinute = calendar.component(.minute, from: startTime)
        var stopHour = calendar.component(.hour, from: stopTime)
        let stopMinute = calendar.component(.minute, from: stopTime)
        if stopHour < startHour || (stopHour == startHour && stopMinute < startMinute) {
            stopHour += 24
        }

        return Double(stopHour * 60 + stopMinute - startHour * 60 - startMinute - 120)
    }
    
    static func removeNotification() -> Void {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        } else {
            UIApplication.shared.cancelAllLocalNotifications()
        }
    }

}
