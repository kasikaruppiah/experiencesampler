//
//  RealmDBObject.swift
//  LEDlab
//
//  Created by LEDlab on 6/28/17.
//  Copyright © 2017 LEDlab. All rights reserved.
//

import Foundation
import RealmSwift

class Response: Object {
    dynamic var tag = ""
    dynamic var value = ""
}

class Survey: Object {
    dynamic var surveyID = 0
    dynamic var createdDate = NSDate()
    dynamic var uploadDate: NSDate? = nil
    let responses = List<Response>()
    
    override static func primaryKey() -> String? {
        return "surveyID"
    }
}
