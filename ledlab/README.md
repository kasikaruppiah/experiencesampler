# README #

### What is this repository for? ###

	iOS mobile application to collect samples from participants and upload it to google sheets

### How do I get set up? ###

	# Configuration
		* xCode 8
		* Swift 3
		* jSON
		* App Script
		* google sheets
	# Dependencies
		* Eureka
		* Swiftyjson
		* Realm
		* Alamofire

### Who do I talk to? ###

	* Kasi Karuppiah Alagappan (alagappan.k@husky.neu.edu)
	* Kimberly Livingstone
	* Prof. Derek Isaacowitz