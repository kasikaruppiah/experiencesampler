package edu.northeastern.ledlab.experiencesampler

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.content.FileProvider
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_upload.*
import java.io.File
import java.io.FileOutputStream


/**
 * Created by ledlab on 8/15/17.
 */
class UploadFragment : android.support.v4.app.Fragment() {
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val uploadMenu = inflater!!.inflate(R.layout.fragment_upload, container, false)

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val participantId = sharedPref.getString(getString(R.string.pid), null)

        uploadMenu.findViewById<Button>(R.id.upload_data).setOnClickListener({ view ->
            kotlin.run {
                val loadingIntent = Intent(context, LoadingActivity::class.java)
                loadingIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                loadingIntent.putExtra("uploadResponse", true)
                startActivity(loadingIntent)
            }
        })

        uploadMenu.findViewById<Button>(R.id.email_data).setOnClickListener({ view ->
            kotlin.run {
                val csv = DataUtils.emailSurveyData()
                val csvFileName = "$participantId.csv"
                val responseCSV = File(context.filesDir, "responses" + File.separator + csvFileName)

                try {
                    responseCSV.parentFile.mkdirs()
                    val fileOutputStream = FileOutputStream(responseCSV)
                    fileOutputStream.write(csv.toByteArray())
                    fileOutputStream.close()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                val intent = Intent(Intent.ACTION_SEND)
                intent.type = "text/comma-separated-values"
                intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(resources.getString(R.string.email_id)))
                intent.putExtra(Intent.EXTRA_SUBJECT, "$participantId - Survey Responses")

                val htmlString = "<p>Hi,</p>" +
                        "<p>Please find attached csv($csvFileName) containing my survey responses.</p>" +
                        "<p>Thanks,<br>$participantId</p>"
                intent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(htmlString))

                val attachmentURI = FileProvider.getUriForFile(context, "edu.northeastern.ledlab.experiencesampler.fileprovider", responseCSV)
                intent.putExtra(Intent.EXTRA_STREAM, attachmentURI)
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

                if (intent.resolveActivity(view.context.packageManager) != null)
                    startActivity(intent)
            }
        })

        return uploadMenu
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            updateStats()
        }
    }

    private fun updateStats() {
        val realm = Realm.getDefaultInstance()
        val totalSurveys = DBUtils.getTotalSurveyCount(realm)
        email_data.isEnabled = totalSurveys > 0
        val offlineSurveys = DBUtils.getOfflineSurveyCount(realm)
        upload_data.isEnabled = offlineSurveys > 0
        realm.close()
        total_surveys.text = String.format(getString(R.string.total), totalSurveys)
        offline_surveys.text = String.format(getString(R.string.offline), offlineSurveys)
    }

    override fun onResume() {
        super.onResume()
        updateStats()
    }
}