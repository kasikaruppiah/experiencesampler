package edu.northeastern.ledlab.experiencesampler

import android.content.Context
import android.content.res.TypedArray
import android.support.v7.preference.DialogPreference
import android.util.AttributeSet

class TimePreference @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = R.attr.preferenceStyle, defStyleRes: Int = defStyleAttr) : DialogPreference(context, attrs, defStyleAttr, defStyleRes) {

    var time: Int = 0
        set(time) {
            field = time
            persistInt(time)
        }

    private val mDialogLayoutResId = R.layout.preference_dialog_time_picker

    override fun onGetDefaultValue(a: TypedArray?, index: Int): Any {
        return a!!.getInt(index, 0)
    }

    override fun getDialogLayoutResource(): Int {
        return mDialogLayoutResId
    }

    override fun onSetInitialValue(restorePersistedValue: Boolean, defaultValue: Any?) {
        time = if (restorePersistedValue)
            getPersistedInt(time)
        else
            defaultValue as Int
    }

}