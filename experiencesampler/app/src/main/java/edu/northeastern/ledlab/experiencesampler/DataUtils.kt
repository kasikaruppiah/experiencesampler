package edu.northeastern.ledlab.experiencesampler

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.preference.PreferenceManager
import android.widget.Toast
import com.google.gson.Gson
import io.realm.Realm
import okhttp3.*
import okhttp3.Response
import java.io.IOException
import java.util.*

/**
 * Created by ledlab on 8/20/17.
 */
object DataUtils {
    fun registerUser(activity: Activity) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(activity)
        val participantId = sharedPref.getString(activity.getString(R.string.pid), "unknown")
        val loginDate = sharedPref.getString(activity.getString(R.string.login_date), android.text.format.DateFormat.format("MM/dd/yyyy HH:mm:ss", Calendar.getInstance().time).toString())
        val deviceInfo = DeviceInfo(participantId, loginDate, Build.VERSION.CODENAME, Build.VERSION.INCREMENTAL, Build.VERSION.RELEASE, Build.VERSION.SDK_INT, Build.BOARD, Build.BOOTLOADER, Build.BRAND, Build.DEVICE, Build.DISPLAY, Build.FINGERPRINT, Build.HARDWARE, Build.HOST, Build.ID, Build.MANUFACTURER, Build.MODEL, Build.PRODUCT, Build.TAGS, Build.TIME, Build.TYPE, Build.USER)

        val json = Gson().toJson(deviceInfo)
        val body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), json)

        val request = Request.Builder().url("https://script.google.com/macros/s/AKfycbwfQgr4fRfcN3se80U4gT0Fp7fs7BhRqbTdkzWoHuhsjnhoh1k/exec").post(body).build()

        OkHttpClient().newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call?, e: IOException?) {
                updateUI(activity, activity.getText(R.string.reg_req_failure) as String)
            }

            override fun onResponse(call: Call?, response: Response?) {
                if (response!!.isSuccessful) {
                    val scriptResponse = Gson().fromJson(response.body()!!.string(), ScriptResponse::class.java)
                    val sharedPref = PreferenceManager.getDefaultSharedPreferences(activity)
                    val prefEditor = sharedPref.edit()
                    prefEditor.putBoolean(activity.getString(R.string.preg), scriptResponse.status)
                    prefEditor.apply()
                    if (scriptResponse.status) {
                        activity.runOnUiThread({
                            SyncJob.scheduleJob()
                            NotifyJob.scheduleJobs(activity)
                            Toast.makeText(activity, activity.getText(R.string.reg_success) as String, Toast.LENGTH_SHORT).show()
                            val intent = Intent(activity, MainActivity::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                            activity.startActivity(intent)
                            activity.finish()
                        })
                    } else updateUI(activity, activity.getText(R.string.reg_failed) as String)
                } else {
                    updateUI(activity, activity.getText(R.string.reg_failed) as String)
                }
            }
        })
    }

    fun emailSurveyData(): String {
        val realm = Realm.getDefaultInstance()
        var csvString = "Survey ID,Survey Date,Tag,Response\n"
        val surveys = DBUtils.getDataForEmail(realm)
        for (survey in surveys!!) {
            val surveyID = survey.surveyID
            val createdDate = android.text.format.DateFormat.format("MM/dd/yyyy HH:mm:ss", survey.createdDate)
            for (response in survey.responses) {
                csvString += "\"$surveyID\",\"$createdDate\",\"${response.tag}\",\"${response.value.replace("\"", "")}\"\n"
            }
        }
        realm.close()

        return csvString
    }

    fun uploadSurveyData(context: Context, update: Boolean) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val pReg = sharedPref.getBoolean(context.getString(R.string.preg), false)

        if (pReg) {
            val realm = Realm.getDefaultInstance()
            val surveys = DBUtils.getDataForUpload(realm)
            if (surveys!!.size > 0) {
                val participantId = sharedPref.getString(context.getString(R.string.pid), "unknown")
                val surveyIds = ArrayList<Long>()
                val surveyResponses = SurveyResponses(participantId, ArrayList())
                for (survey in surveys) {
                    val surveyID = survey.surveyID
                    surveyIds.add(surveyID)
                    val createdDate = android.text.format.DateFormat.format("MM/dd/yyyy HH:mm:ss", survey.createdDate)
                    val responses = survey.responses.mapTo(ArrayList()) { hashMapOf(Pair("tag", it.tag), Pair("value", it.value)) }
                    surveyResponses.surveys.add(SurveyResponse(surveyID, createdDate.toString(), responses))
                }

                val json = Gson().toJson(surveyResponses)

                val client = OkHttpClient()
                val body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), json)
                val request = Request.Builder().url("https://script.google.com/macros/s/AKfycbxXYgdKvpWRZpLsn22rdE9WRgOpzEeAqye6ehX8UW69Ojj1MuEk/exec").post(body).build()

                client.newCall(request).enqueue(object : Callback {
                    override fun onFailure(call: Call?, e: IOException?) {
                        if (update) updateUI(context, context.getString(R.string.upld_req_failure))
                    }

                    override fun onResponse(call: Call?, response: Response?) {
                        var msg = ""
                        msg = if (response!!.isSuccessful) {
                            val scriptResponse = Gson().fromJson(response.body()!!.string(), ScriptResponse::class.java)
                            if (scriptResponse.status) {
                                DBUtils.updateUploadDate(surveyIds.toArray(Array(surveyIds.size, { i -> 0L })))
                                context.getString(R.string.upld_success)
                            } else context.getString(R.string.upld_failed)
                        } else {
                            context.getString(R.string.upld_failed)
                        }
                        if (update) updateUI(context, msg)
                    }
                })
            } else {
                if (update) updateUI(context, context.getString(R.string.no_res))
            }
            realm.close()
        }
    }

    fun updateUI(context: Context, msg: String) {
        val activity = context as Activity
        activity.runOnUiThread({
            Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
            activity.finish()
        })
    }
}