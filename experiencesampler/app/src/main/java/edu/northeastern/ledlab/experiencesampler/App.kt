package edu.northeastern.ledlab.experiencesampler

import android.app.Application
import android.os.Build
import com.evernote.android.job.JobManager
import io.realm.Realm

/**
 * Created by ledlab on 9/1/17.
 */
class App : Application() {
    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
        JobManager.create(this).addJobCreator(JARVIS())
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N)
            JobManager.instance().config.isAllowSmallerIntervalsForMarshmallow = true
    }
}
