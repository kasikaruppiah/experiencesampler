package edu.northeastern.ledlab.experiencesampler

import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by ledlab on 9/3/17.
 */
class ExecutionWindow(
        targetDay: Int,
        targetHour: Int,
        targetMinute: Int,
        windowLengthInMinutes: Int? = 1) {
    var startMs = 0L
    var endMs = 0L

    init {
        val calendar = Calendar.getInstance()
        val currentDay = calendar.get(Calendar.DAY_OF_WEEK)
        val currentHour = calendar.get(Calendar.HOUR_OF_DAY)
        val currentMinute = calendar.get(Calendar.MINUTE)

        var dayOffset = TimeUnit.DAYS.toMillis((targetDay - currentDay).toLong())
        val hourOffset = TimeUnit.HOURS.toMillis((targetHour - currentHour).toLong())
        val minuteOffset = TimeUnit.MINUTES.toMillis((targetMinute - currentMinute).toLong())

        if ((targetDay < currentDay) or
                ((targetDay == currentDay) and (targetHour < currentHour)) or
                ((targetDay == currentDay) and (targetHour == currentHour) and (targetMinute <=
                        currentMinute)))
            dayOffset += TimeUnit.DAYS.toMillis(7)

        startMs = Math.max(dayOffset + hourOffset + minuteOffset, 60000)
        endMs = startMs + TimeUnit.MINUTES.toMillis(windowLengthInMinutes!!.toLong())
    }
}