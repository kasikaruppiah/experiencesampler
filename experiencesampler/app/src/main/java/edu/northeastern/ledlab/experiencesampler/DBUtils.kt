package edu.northeastern.ledlab.experiencesampler

import io.realm.Realm
import io.realm.RealmResults
import java.util.*

/**
 * Created by ledlab on 8/20/17.
 */
object DBUtils {

    fun addSurvey(userResponse: HashMap<String, String>) {
        val realm = Realm.getDefaultInstance()
        realm.use { realm ->
            realm.executeTransaction {
                val maxValue = realm.where(Survey::class.java).max("surveyID")
                var surveyID = 1
                if (maxValue != null) surveyID = maxValue.toInt() + 1
                val newSurvey = realm.createObject(Survey::class.java, surveyID)

                for ((tag, value) in userResponse) {
                    val response = realm.createObject(Response::class.java)
                    response.tag = tag
                    response.value = value
                    newSurvey.responses.add(response)
                }
            }
        }
    }

    fun getDataForEmail(realm: Realm?): RealmResults<Survey>? {
        return realm!!.where(Survey::class.java).findAll()
    }

    fun getTotalSurveyCount(realm: Realm?): Int {
        return realm!!.where(Survey::class.java).findAll().size
    }

    fun getOfflineSurveyCount(realm: Realm?): Int {
        return realm!!.where(Survey::class.java).isNull("uploadDate").findAll().size
    }

    fun getDataForUpload(realm: Realm?): RealmResults<Survey>? {
        return realm!!.where(Survey::class.java).isNull("uploadDate").findAll()
    }

    fun updateUploadDate(surveyIds: Array<Long>) {
        val realm = Realm.getDefaultInstance()
        realm!!.executeTransaction({ realm ->
            kotlin.run {
                val surveys = realm.where(Survey::class.java).`in`("surveyID", surveyIds).findAll()
                for (survey in surveys) {
                    survey.uploadDate = Calendar.getInstance().time
                }
            }
        })
        realm.close()
    }
}