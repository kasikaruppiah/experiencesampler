package edu.northeastern.ledlab.experiencesampler

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.widget.TextViewCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.google.gson.GsonBuilder
import com.google.gson.stream.JsonReader
import kotlinx.android.synthetic.main.activity_questionnaire.*
import java.io.InputStreamReader
import java.util.*
import java.util.concurrent.atomic.AtomicInteger

/**
 * Created by ledlab on 8/16/17.
 */
class QuestionnaireActivity : AppCompatActivity() {

    private lateinit var survey: Questionnaire
    private lateinit var currentQuestion: Question
    private val params = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    private val userResponse = HashMap<String, String>()

    init {
        params.setMargins(16, 16, 16, 16)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_questionnaire)

        val inputStream = resources.openRawResource(R.raw.survey)
        survey = GsonBuilder().create().fromJson<Questionnaire>(JsonReader(InputStreamReader(inputStream, "UTF-8")), Questionnaire::class.java)

        val textLabel = TextView(this)
        textLabel.id = R.id.text_label
        textLabel.layoutParams = params
        TextViewCompat.setTextAppearance(textLabel, android.R.style.TextAppearance_Medium)
        questionnaire.addView(textLabel)

        val nextButton = Button(this)
        nextButton.id = R.id.next_button
        nextButton.tag = "next_button"
        nextButton.layoutParams = params
        questionnaire.addView(nextButton)

        loadQuestion(0)
    }

    private fun loadQuestion(questionId: Int) {
        val textLabel = this.findViewById<TextView>(R.id.text_label)
        val nextButton = findViewById<Button>(R.id.next_button)

        if (questionId >= survey.questions.size) {
            textLabel.text = getText(R.string.upld_msg)
            nextButton.text = getText(R.string.finish)
            nextButton.setOnClickListener { view ->
                kotlin.run {
                    DBUtils.addSurvey(userResponse)
                    val loadingIntent = Intent(this, LoadingActivity::class.java)
                    loadingIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                    loadingIntent.putExtra("uploadResponse", true)
                    startActivity(loadingIntent)
                    finish()
                }
            }
        } else {
            currentQuestion = survey.questions[questionId]
            textLabel.text = currentQuestion.question

            when (currentQuestion.type) {
                "radio" -> {
                    nextButton.isEnabled = false
                    addRadioButton()
                }
                "checkbox" -> {
                    addCheckBox()
                }
                "text" -> {
                    nextButton.isEnabled = false
                    addEditText()
                }
            }

            nextButton.text = getText(R.string.next)
            nextButton.setOnClickListener { view -> evaluateResponse(questionId) }
        }
        questionnaireScrollView.post({ questionnaireScrollView.smoothScrollTo(0, 0) })
    }

    private fun addRadioButton() {
        val radioGroup = RadioGroup(this)
        radioGroup.id = R.id.response
        radioGroup.layoutParams = params
        for (option in currentQuestion.options) {
            val radioButton = RadioButton(this)
            if (Build.VERSION.SDK_INT < 17)
                radioButton.id = generateViewId()
            if (!option.image.isNullOrEmpty()) {
                radioButton.setCompoundDrawablesWithIntrinsicBounds(
                        resources.getIdentifier(option.image, "drawable", this.packageName),
                        0, 0, 0)
                radioButton.compoundDrawablePadding = 16
                var left = 0
                if (Build.VERSION.SDK_INT < 17)
                    left = 32
                radioButton.setPadding(left, 8, 0, 8)
            }
            radioButton.text = option.option
            radioButton.tag = option.value
            TextViewCompat.setTextAppearance(radioButton, android.R.style.TextAppearance_Medium)
            radioGroup.addView(radioButton)
        }
        radioGroup.setOnCheckedChangeListener { radioGroup, i ->
            findViewById<Button>(R.id.next_button).isEnabled = true
        }
        questionnaire.addView(radioGroup, 1)
    }

    private fun addCheckBox() {
        val linearLayout = LinearLayout(this)
        linearLayout.id = R.id.response
        linearLayout.layoutParams = params
        linearLayout.orientation = LinearLayout.VERTICAL
        for (option in currentQuestion.options) {
            val checkBox = CheckBox(this)
            checkBox.text = option.option
            checkBox.tag = option.value
            TextViewCompat.setTextAppearance(checkBox, android.R.style.TextAppearance_Medium)
            linearLayout.addView(checkBox)
        }
        questionnaire.addView(linearLayout, 1)
    }

    private fun addEditText() {
        val editText = EditText(this)
        editText.id = R.id.response
        editText.layoutParams = params
        editText.inputType = InputType.TYPE_TEXT_FLAG_MULTI_LINE
        editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                //TODO("not implemented") //To change body of created functions use File |
                // Settings | File Templates.
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                //TODO("not implemented") //To change body of created functions use File |
                // Settings | File Templates.
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val nextButton = findViewById<Button>(R.id.next_button)
                nextButton.isEnabled = p0.toString().trim().isNotEmpty()
            }
        })
        questionnaire.addView(editText, 1)
    }

    private fun evaluateResponse(questionId: Int) {
        val responses = ArrayList<String>()
        var response = ""
        when (currentQuestion.type) {
            "radio" -> {
                val radioGroup = findViewById<RadioGroup>(R.id.response)
                val selectedId = radioGroup.checkedRadioButtonId
                response = findViewById<RadioButton>(selectedId).tag.toString()
                responses.add(response)
            }
            "checkbox" -> {
                val linearLayout = findViewById<LinearLayout>(R.id.response)
                (0 until linearLayout.childCount)
                        .map { linearLayout.getChildAt(it) as CheckBox }
                        .filter { it.isChecked }
                        .mapTo(responses) { it.tag.toString() }
                response = responses.joinToString(", ")
            }
            "text" -> {
                hideSoftInput()
                val editText = findViewById<EditText>(R.id.response)
                response = editText.text.toString()
            }
        }
        userResponse.put(currentQuestion.tag, response)
        clearResponse()
        var nextQuestion = questionId + 1
        if (currentQuestion.nextquestion == 0
                && currentQuestion.type in listOf("radio", "checkbox")
                && currentQuestion.conditions != null) {
            for ((value, nextquestion) in currentQuestion.conditions) {
                if (value in responses) {
                    nextQuestion = nextquestion
                    break
                }
            }
        } else if (currentQuestion.nextquestion != 0) {
            nextQuestion = currentQuestion.nextquestion
        }
        loadQuestion(nextQuestion)
    }

    private fun hideSoftInput() {
        val view = this.currentFocus
        if (view != null) {
            val iMM = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            iMM.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private fun clearResponse() {
        questionnaire.removeViewAt(1)
    }

    override fun onBackPressed() {
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setMessage("All unsaved progress will be lost")
        alertDialog.setPositiveButton("Ok") { p0, p1 -> finish() }
        alertDialog.setNegativeButton("Cancel") { p0, p1 -> }
        alertDialog.show()
    }

    private val sNextGeneratedId = AtomicInteger(1)
    private fun generateViewId(): Int {
        while (true) {
            val result = sNextGeneratedId.get()
            // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
            var newValue = result + 1
            if (newValue > 0x00FFFFFF)
                newValue = 1 // Roll over to 1, not 0.
            if (sNextGeneratedId.compareAndSet(result, newValue))
                return result
        }
    }
}