package edu.northeastern.ledlab.experiencesampler

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.preference.PreferenceManager
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat
import com.evernote.android.job.Job
import com.evernote.android.job.JobRequest
import com.evernote.android.job.util.support.PersistableBundleCompat

/**
 * Created by ledlab on 9/2/17.
 */
class NotifyJob : Job() {
    override fun onRunJob(params: Job.Params?): Job.Result {
        val extras = params!!.extras
        try {
            val mBuilder = NotificationCompat.Builder(context).
                    setSmallIcon(R.drawable.ic_launcher).
                    setContentTitle(context.getString(R.string.app_name)).
                    setContentText(extras.getString("text", "Time for Survey")).
                    setColor(ContextCompat.getColor(context, R.color.colorPrimary)).
                    setAutoCancel(true)

            val resumeIntent = Intent(context, MainActivity::class.java)
            val stackBuilder = android.support.v4.app.TaskStackBuilder.create(context)
            stackBuilder.addParentStack(MainActivity::class.java)
            stackBuilder.addNextIntent(resumeIntent)
            val resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
            mBuilder.setContentIntent(resultPendingIntent)
            val mNotificationId = System.currentTimeMillis().toInt()
            val mNotifyMgr = context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            mNotifyMgr.notify(mNotificationId, mBuilder.build())

            return Job.Result.SUCCESS
        } finally {
            scheduleJob(extras.getInt("day", 0),
                    extras.getInt("time", 0),
                    extras.getString("text", "Time for Survey"))
        }
    }

    companion object {
        val TAG = "job_notify_db"

        fun scheduleJob(day: Int, time: Int, text: String? = "Time for Survey") {
            val extras = PersistableBundleCompat()
            extras.putInt("day", day)
            extras.putInt("time", time)
            extras.putString("text", text)

            val hour = time / 60
            val minute = time % 60
            val executionWindow = ExecutionWindow(day, hour, minute)

            JobRequest.
                    Builder(NotifyJob.TAG).
                    setExecutionWindow(executionWindow.startMs, executionWindow.endMs).
                    setExtras(extras).
                    setPersisted(true).
                    build().
                    schedule()
        }

        fun scheduleJobs(context: Context) {
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
            val weekdayWake = sharedPref.getInt(context.getString(R.string.weekday_wake_key), 360)
            val weekdaySleep = sharedPref.getInt(context.getString(R.string.weekday_sleep_key), 1320)
            val weekendWake = sharedPref.getInt(context.getString(R.string.weekend_wake_key), 480)
            val weekendSleep = sharedPref.getInt(context.getString(R.string.weekend_sleep_key), 0)

            val weekdayDiff = timeDiff(weekdayWake, weekdaySleep)
            val weekendDiff = timeDiff(weekendWake, weekendSleep)
            val weekdayWeekendDiff = timeDiff(weekdayWake, weekendSleep)
            val weekendWeekDayDiff = timeDiff(weekendWake, weekdaySleep)

            for (i in 1..7) {
                var newTime = weekdayWake
                if (i in listOf(1, 7))
                    newTime = weekendWake

                var timeDiff = weekdayDiff
                when (i) {
                    6 -> timeDiff = weekdayWeekendDiff
                    7 -> timeDiff = weekendDiff
                    1 -> timeDiff = weekendWeekDayDiff
                }
                for (j in 1..5) {
                    newTime += if (j == 1)
                        60
                    else
                        timeDiff
                    scheduleJob(i, newTime, "Time for your next survey ($j / 5)")
                }
            }
        }

        private fun timeDiff(wakeTime: Int, sleepTime: Int): Int {
            val wakeHour = wakeTime / 60
            val wakeMinute = wakeTime % 60
            var sleepHour = sleepTime / 60
            val sleepMinute = sleepTime % 60

            if ((sleepHour < wakeHour) or ((sleepHour == wakeHour) and (sleepMinute <= wakeMinute)))
                sleepHour += 24

            return (sleepHour * 60 + sleepMinute - wakeHour * 60 - wakeMinute - 120) / 4
        }
    }
}