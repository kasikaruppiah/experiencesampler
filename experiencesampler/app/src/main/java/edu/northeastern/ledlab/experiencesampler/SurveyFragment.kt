package edu.northeastern.ledlab.experiencesampler

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

/**
 * Created by ledlab on 8/15/17.
 */
class SurveyFragment : android.support.v4.app.Fragment() {
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val surveyMenu = inflater!!.inflate(R.layout.fragment_survey, container, false)
        surveyMenu.findViewById<Button>(R.id.start_survey).setOnClickListener({ view ->
            kotlin.run {
                val surveyIntent = Intent(view.context, QuestionnaireActivity::class.java)
                startActivity(surveyIntent)
            }
        })
        return surveyMenu
    }
}