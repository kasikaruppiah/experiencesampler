package edu.northeastern.ledlab.experiencesampler

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

/**
 * Created by ledlab on 8/20/17.
 */
open class Response(
        var tag: String = "",
        var value: String = ""
) : RealmObject()

open class Survey(
        @PrimaryKey var surveyID: Long = 0,
        var createdDate: Date = Calendar.getInstance().time,
        var uploadDate: Date? = null,
        var responses: RealmList<Response> = RealmList()
) : RealmObject()