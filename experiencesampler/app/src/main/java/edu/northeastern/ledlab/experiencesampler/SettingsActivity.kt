package edu.northeastern.ledlab.experiencesampler

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

/**
 * Created by ledlab on 8/15/17.
 */
class SettingsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_settings)
    }
}