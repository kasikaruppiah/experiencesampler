package edu.northeastern.ledlab.experiencesampler

import java.util.*

/**
 * Created by ledlab on 8/23/17.
 */

data class DeviceInfo(
        val participantid: String,
        val logindate: String,
        val codename: String,
        val incremental: String,
        val release: String,
        val sdk: Int,
        val board: String,
        val bootloader: String,
        val brand: String,
        val device: String,
        val display: String,
        val fingerprint: String,
        val hardware: String,
        val host: String,
        val id: String,
        val manufacturer: String,
        val model: String,
        val product: String,
        val tags: String,
        val time: Long,
        val type: String,
        val user: String
)

data class SurveyResponses(
        val participantid: String,
        val surveys: ArrayList<SurveyResponse>
)

data class SurveyResponse(
        val surveyid: Long,
        var createddate: String,
        var responses: ArrayList<HashMap<String, String>>
)

data class ScriptResponse(val status: Boolean)