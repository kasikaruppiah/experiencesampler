package edu.northeastern.ledlab.experiencesampler

import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.evernote.android.job.JobManager
import kotlinx.android.synthetic.main.activity_loading.*


/**
 * Created by ledlab on 8/23/17.
 */
class LoadingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loading)
        when {
            intent.getBooleanExtra("registerUser", false) -> {
                loading_text.text = getString(R.string.loading_reg_txt)
                DataUtils.registerUser(this)
            }
            intent.getBooleanExtra("uploadResponse", false) -> {
                loading_text.text = getString(R.string.loading_upld_txt)
                DataUtils.uploadSurveyData(this, true)
            }
            intent.getBooleanExtra("updateNotification", false) -> {
                loading_text.text = getString(R.string.updt_notification)
                val addNotification = intent.getBooleanExtra("addNotification", false)
                val updateNotification = UpdateNotification()
                updateNotification.execute(addNotification)
            }
        }
    }

    override fun onBackPressed() {}

    private inner class UpdateNotification : AsyncTask<Boolean, Unit, Unit>() {
        override fun doInBackground(vararg p0: Boolean?) {
            JobManager.instance().cancelAllForTag(NotifyJob.TAG)
            if (p0[0]!!)
                NotifyJob.scheduleJobs(this@LoadingActivity)
        }

        override fun onPostExecute(result: Unit?) {
            super.onPostExecute(result)
            finish()
        }
    }
}