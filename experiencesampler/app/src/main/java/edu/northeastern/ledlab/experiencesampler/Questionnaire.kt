package edu.northeastern.ledlab.experiencesampler

/**
 * Created by ledlab on 8/19/17.
 */
data class Questionnaire(
        val questions: Array<Question>
)

data class Question(
        val type: String,
        val tag: String,
        val question: String,
        val options: Array<Option>,
        val conditions: Array<Condition>,
        val nextquestion: Int,
        val required: Boolean
)

data class Option(
        val option: String,
        val image: String,
        val value: String
)

data class Condition(
        val value: String,
        val nextquestion: Int
)