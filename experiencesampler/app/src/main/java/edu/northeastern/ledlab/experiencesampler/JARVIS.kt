package edu.northeastern.ledlab.experiencesampler

import com.evernote.android.job.Job
import com.evernote.android.job.JobCreator


/**
 * Created by ledlab on 9/1/17.
 */
class JARVIS : JobCreator {
    override fun create(tag: String?): Job? {
        return when (tag) {
            SyncJob.TAG -> SyncJob()
            NotifyJob.TAG -> NotifyJob()
            else -> null
        }
    }
}